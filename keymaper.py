import lookup
import sys
import json
import yaml


# convert the json format with unicode to the kbdmod format with evdev
def convert_action(action, table):
    if type(action) is str:
        return table[action]
    elif type(action) is list:
        return [convert_action(a, table) for a in action]
    elif type(action) is dict:
        action_kbdmod = {}
        for action_type, action_value in action.items():
            if action_type == "standard_output":
                action_kbdmod["standard_output"] = convert_action(action_value, table)
            else:
                action_kbdmod[action_type] = action_value
        if action_kbdmod.__len__() == 1:
            return action_kbdmod
        else:
            action_map = {}
            action_map["multiple_commands"] = action_kbdmod
            return action_map


if sys.argv.__len__() != 3:
    print("Usage: keymaper.py <layout_code> <layout_type> to get possible unicode characters for a given layout code and layout type.")
    print("keymaper.py <input.json> <output.yaml> to generate kbdmod configuration file from input json file.")
    sys.exit(1)

if sys.argv[1].endswith('.json') and sys.argv[2].endswith('.yaml'):
    input_file = sys.argv[1]
    output_file = sys.argv[2]
    with open(input_file, 'r') as file:
        data = json.load(file)
    table_in = lookup.get_lookup_table(data['layout_code_in'], data['layout_type_in'])
    table_out = lookup.get_lookup_table(data['layout_code_out'], data['layout_type_out'])

    output_data = {}
    if data.get('hold_time') is not None:
        output_data['hold_time'] = data['hold_time']
    if data.get('autoshift') is not None:
        output_data['autoshift'] = data['autoshift']

    output_data['layers'] = {}
    for layer, keys in data['layers'].items():
        output_data['layers'][layer] = []
        for key_label, key_prop in keys.items():
            key_dict = {}
            if key_label == 'comment':
                continue
            key_dict['key'] = table_in[key_label]
            if key_prop.get('tap'):
                key_dict['tap'] = convert_action(key_prop['tap'], table_out)
            if key_prop.get('hold'):
                key_dict['hold'] = convert_action(key_prop['hold'], table_out)
            if key_prop.get('doubletap'):
                key_dict['doubletap'] = convert_action(key_prop['doubletap'], table_out)
            if key_prop.get('taphold'):
                key_dict['taphold'] = convert_action(key_prop['taphold'], table_out)

            if key_prop.get('tap_osm'):
                key_dict['tap'] = convert_action(key_prop['tap_osm'], table_out)
                key_dict['tap_osm'] = True
            if key_prop.get('hold_osm'):
                key_dict['hold'] = convert_action(key_prop['hold_osm'], table_out)
                key_dict['hold_osm'] = True
            if key_prop.get('doubletap_osm'):
                key_dict['doubletap'] = convert_action(key_prop['doubletap_osm'], table_out)
                key_dict['doubletap_osm'] = True
            if key_prop.get('taphold_osm'):
                key_dict['taphold'] = convert_action(key_prop['taphold_osm'], table_out)
                key_dict['taphold_osm'] = True

            if key_dict.__len__() > 1:
                output_data['layers'][layer].append(key_dict)
    with open(output_file, 'w') as file:
        yaml.dump(output_data, file)
    exit(0)
else:
    layout_code = sys.argv[1]
    layout_type = sys.argv[2]

    table = lookup.get_lookup_table(layout_code, layout_type)
    print(f'keys found {table.__len__()}')
    print('valid keys are:')
    for key, value in table.items():
        print(key, end=', ')
    print()
    exit(0)
