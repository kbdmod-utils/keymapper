import os
import re
import sys


def find_layout_file(layout_code):
    xkb_dir = '/usr/share/X11/xkb'
    symbols_dir = os.path.join(xkb_dir, 'symbols')

    layout_file = f"{layout_code.lower()}"
    layout_file_path = os.path.join(symbols_dir, layout_file)

    return layout_file_path


def parse_keymap(filename, layout_type):
    keycode_to_unicode = {}
    layout_found = False

    try:
        # Read the provided keymap file
        with open(filename, 'r') as file:
            lines = file.readlines()

        # Regex pattern to extract keycode and Unicode mappings
        pattern = re.compile(r'\bkey\s+<(\w+)>\s*{\s*\[\s*([\w\s,]+)\s*]\s*};')

        for line in lines:
            # break on new layout type declaration
            match = re.search(r'xkb_symbols\s+"(\b(?:\w+-)*\w+\b)"', line)
            if match and layout_found:
                return keycode_to_unicode
            if match and match.group(1) == layout_type:
                print(f"Found layout type {layout_type} in {filename}.")
                layout_found = True
            match = pattern.search(line)
            if match:
                keycode = match.group(1)
                unicode = match.group(2)
                unicode = unicode.split(' ')[0].rstrip('\t').rstrip(',')
                keycode_to_unicode[keycode] = unicode

        if not layout_found:
            print(f"Error: Layout type {layout_type} not found in {filename}.")
            return {}
        return keycode_to_unicode

    except FileNotFoundError:
        print(f"Error: {filename} not found.")
        return {}

def unicode_to_int(keycode_to_unicode):
    unicode_to_evdev = {}
    keycode_to_evdev = {}
    lookup_file = '/usr/share/X11/xkb/keycodes/evdev'
    with open(lookup_file, 'r') as file:
        lines = file.readlines()
        # 8 is the offset for evdev keycodes
        # you have to subtract it look at the evdev file for reference
        subtract = 8
        for line in lines:
            # regrex declaration
            pattern = r'\s*<(\w+)>\s*=\s*(\d+);'
            match = re.match(pattern, line)
            if match:
                tag = match.group(1)
                value = int(match.group(2))
                keycode_to_evdev[tag] = value - subtract
            # regex alias
            pattern = r'\s*alias\s+<(\w+)>\s*=\s*<(\w+)>;.*'
            match = re.match(pattern, line)
            if match:
                tag = match.group(2)
                alias = match.group(1)
                if tag in keycode_to_evdev:
                    value = keycode_to_evdev[tag]
                    keycode_to_evdev[alias] = value
    for key, value in keycode_to_unicode.items():
        if key in keycode_to_evdev:
            unicode_to_evdev[value] = keycode_to_evdev[key]

    return unicode_to_evdev


def get_lookup_table(layout_code, layout_type):

    # Step 1: Find the layout file corresponding to the layout code
    layout_file_path = find_layout_file(layout_code)
    base_key = '/usr/share/X11/xkb/symbols/digital_vndr/pc'
    if not os.path.exists(layout_file_path):
        print(f"Error: Layout file not found for layout code {layout_code}.")
        sys.exit(1)

    # Step 2: Parse the located keymap file
    keycode_to_unicode = parse_keymap(layout_file_path, layout_type)
    keycode_to_unicode.update(parse_keymap(base_key, 'pc104'))

    # Step 3: reverse the dictionary and resolve the evdev keycode
    unicode_to_evdev = unicode_to_int(keycode_to_unicode)

    return unicode_to_evdev

